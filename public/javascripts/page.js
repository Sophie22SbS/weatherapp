"use strict";
import {$} from "./modules/nQuery.js";
import {Ajax} from "./modules/Ajax.js";

//Jeg har brugt weatherapi i stedet for openweathermap, da jeg så denne video:
//https://www.youtube.com/watch?v=01WtuEiF4G8
//I videoens beskrivelse er der hans github, med alt koden fra videoen.
const API_KEY = `06609db6bbd9404db2d151647220203`;
const API_URL = `http://api.weatherapi.com/v1/current.json?key=${API_KEY}`;

/*
 * Event handler for button - create ajax object and get data
 */
const getContinents = function(ev) {
    let req = Object.create(Ajax);
    req.init();
    req.getFile("/continents", showContinents);
};
const getCountries = function(ev) {
    let req = Object.create(Ajax);
    req.init();
    console.log(ev.target.value);
    req.getFile(`/countries/${ev.target.value}`, showCountries);
};
const getCities = function(ev) {
    let req = Object.create(Ajax);
    req.init();
    req.getFile(`/cities/${ev.target.value}`, showCities);
};
const getCityInfo = function(ev) {
    let req = Object.create(Ajax);
    req.init();
    req.getFile(`/city/${ev.target.value}`, showCityInfo);
};
/*
const showContinents = function(e) {
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("contdata");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Continents');
    h3.appendChild(txt);
    div.appendChild(h3);
    let continents = JSON.parse(e.target.responseText);
    let sel = document.createElement('select');
    sel.setAttribute('id', 'chooseContinent');
    sel.addEventListener('change', getCountries);
    continents.forEach(function(continent) {
        let opt = document.createElement('option');
        let opttext = document.createTextNode(continent.name);
        opt.appendChild(opttext);
        sel.appendChild(opt);
    });
    div.appendChild(sel);
    $("contdata").appendChild(div);
}
*/

const showContinents = function(e) {
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("contdata");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Continents');
    h3.appendChild(txt);
    div.appendChild(h3);
    let continents = JSON.parse(e.target.responseText);
    
    let sel = document.createElement('div');
    sel.setAttribute('id', 'chooseContinent');
    sel.setAttribute('style', 'overflow-y: scroll; height: 200px;');
    continents.forEach(function(continent) {
        let opt = document.createElement('input');
        opt.addEventListener('change', getCountries);
        opt.setAttribute('type', 'radio');
        opt.setAttribute('id', 'idTest');
        opt.setAttribute('name', 'fav_language');
        opt.setAttribute('value', continent.name);
        sel.appendChild(opt);

        let spa = document.createElement('span');
        let opttext = document.createTextNode(continent.name);
        spa.appendChild(opttext);
        sel.appendChild(spa);
        let br = document.createElement('br');
        sel.appendChild(br);
    });
    div.appendChild(sel);
    $("contdata").appendChild(div);
}

/*
const showCountries = function (e) {
    // here you put the ajax response onto your page DOM
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("countdata");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Countries');
    h3.appendChild(txt);
    div.appendChild(h3);
    let countries = JSON.parse(e.target.responseText);
    let sel = document.createElement('select');
    sel.setAttribute('id', 'chooseCountry');
    // sel.addEventListener('change', getCountries);
    countries.forEach(function(country) {
        let opt = document.createElement('option');
        let opttext = document.createTextNode(country.name);
        opt.appendChild(opttext);
        sel.appendChild(opt);
    });
    div.appendChild(sel);
    $("countdata").appendChild(div);
};
 */

const showCountries = function(e) {
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("countdata");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Countries');
    h3.appendChild(txt);
    div.appendChild(h3);
    let countries = JSON.parse(e.target.responseText);
    
    let sel = document.createElement('div');
    sel.setAttribute('id', 'chooseCountry');
    sel.setAttribute('style', 'overflow-y: scroll; height: 200px;');
    countries.forEach(function(country) {
        let opt = document.createElement('input');
        opt.addEventListener('change', getCities);
        opt.setAttribute('type', 'radio');
        opt.setAttribute('id', country.code);
        opt.setAttribute('name', 'code');
        opt.setAttribute('value', country.code);
        sel.appendChild(opt);

        let spa = document.createElement('span');
        let opttext = document.createTextNode(country.name);
        spa.appendChild(opttext);
        sel.appendChild(spa);
        let br = document.createElement('br');
        sel.appendChild(br);
    });
    div.appendChild(sel);
    $("countdata").appendChild(div);
}

const showCities = function(e) {
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("citydata");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('The Cities');
    h3.appendChild(txt);
    div.appendChild(h3);
    let cities = JSON.parse(e.target.responseText);
    
    let sel = document.createElement('div');
    sel.setAttribute('id', 'chooseCity');
    sel.setAttribute('style', 'overflow-y: scroll; height: 200px;');
    cities.forEach(function(city) {
        let opt = document.createElement('input');
        opt.addEventListener('change', getCityInfo);
        opt.setAttribute('type', 'radio');
        opt.setAttribute('id', 'idTest');
        opt.setAttribute('name', 'fav_language');
        opt.setAttribute('value', city.name);
        sel.appendChild(opt);

        let spa = document.createElement('span');
        let opttext = document.createTextNode(city.name);
        spa.appendChild(opttext);
        sel.appendChild(spa);
        let br = document.createElement('br');
        sel.appendChild(br);
    });
    div.appendChild(sel);
    $("citydata").appendChild(div);
}

const showCityInfo = function(e) {
    console.log(e.target.getResponseHeader("Content-Type"));
    let element = $("cityInfoData");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    let div = document.createElement("div");
    let h3 = document.createElement('h3');
    let txt = document.createTextNode('City information');
    h3.appendChild(txt);
    div.appendChild(h3);

    $("cityInfoData").appendChild(div);
    
    /* Dens error er "async unexpected identifier" hvilket jeg ikke forstår.
    async getWeather() {
        const res = await fetch(`${API_URL}&q=${city.name}`)
            .then(response => response.json())

        return res.current.temp_c ;
    }
    */
}

/*
 *  Listen to the get films button
 */
const showStarter = function () {
    $('gcont').addEventListener('click', getContinents);
}

window.addEventListener("load", showStarter);
